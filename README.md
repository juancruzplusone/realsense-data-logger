# RealSense Data Logger

This project includes a GUI to facilitate data logging and visualization for RealSense cameras with different configurations including, FPS, resolution, data size, and channels tested. Generates data logs and plots for each camera in the current directory.

Future work will further automate the process by adding configuration by device.

Can be used to validate USB cable data quality over time.

## Installation

1. Clone the repository to your local machine:
2. Install the required Python libraries by running the following command:

    ```sh
    pip3 install -r requirements.txt
    ```

## Usage

### Running the Camera Test

1. Connect the RealSense camera(s) to your computer.

2. Run the main script to open the interactive GUI:

    ```sh
    python main.py
    ```

3. Follow GUI instructions to select the desired camera configurations and click on Start Test.
4. Click on View Results to view the data logs and plots.

## Note

Make sure that the RealSense camera is connected via the cable you want to test before running the scripts.