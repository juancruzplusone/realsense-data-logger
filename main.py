from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
import os
import sys
from modules.gui_driver import App

if __name__ == '__main__':
        QApplication.setAttribute(Qt.AA_DisableHighDpiScaling)
        app = QApplication(sys.argv)
        icon_path = os.path.join('assets', 'icons8-mobile-suit-gundam-50.png')
        app.setWindowIcon(QIcon(icon_path))
        ex = App()
        ex.show()
        sys.exit(app.exec_())
