from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton, QLineEdit,QCheckBox, QProgressBar, QScrollArea, QFormLayout, QGroupBox, QSizePolicy
from PyQt5.QtGui import QFont
import datetime
from typing import Dict


class ParameterInputInterface(QWidget):
    def __init__(self, parent=None):
        super(ParameterInputInterface, self).__init__(parent)

        self.title = 'Realsense Camera Data Logger'
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)

        layout = QVBoxLayout()

        # Add input fields for each parameter
        self.testDurationInput = QLineEdit()
        self.testDurationInput.setText("0.1")
        layout.addWidget(QLabel("Test Duration (hr):"))
        layout.addWidget(self.testDurationInput)

        self.dataPointsInput = QLineEdit()
        self.dataPointsInput.setText("500")
        layout.addWidget(QLabel("Number of data points to gather:"))
        layout.addWidget(self.dataPointsInput)

        # Add checkboxes for each channel
        self.rgbCheckbox = QCheckBox("RGB")
        self.rgbCheckbox.setChecked(True)
        layout.addWidget(self.rgbCheckbox)

        self.depthCheckbox = QCheckBox("Depth")
        self.depthCheckbox.setChecked(True)
        layout.addWidget(self.depthCheckbox)

        self.infraredCheckbox = QCheckBox("Infrared")
        self.infraredCheckbox.setChecked(True)
        layout.addWidget(self.infraredCheckbox)

        # Add input fields for the remaining parameters
        self.resolutionLInput = QLineEdit()
        self.resolutionLInput.setText("640")
        layout.addWidget(QLabel("Resolution Length (pixels):"))
        layout.addWidget(self.resolutionLInput)

        self.resolutionWInput = QLineEdit()
        self.resolutionWInput.setText("480")
        layout.addWidget(QLabel("Resolution Width (pixels):"))
        layout.addWidget(self.resolutionWInput)

        self.fpsInput = QLineEdit()
        self.fpsInput.setText("30")
        layout.addWidget(QLabel("FPS:"))
        layout.addWidget(self.fpsInput)

        self.depthBitsInput = QLineEdit()
        self.depthBitsInput.setText("16")
        layout.addWidget(QLabel("Depth Bits:"))
        layout.addWidget(self.depthBitsInput)

        self.colorBitsInput = QLineEdit()
        self.colorBitsInput.setText("8")
        layout.addWidget(QLabel("Color Bits:"))
        layout.addWidget(self.colorBitsInput)

        self.infraredBitsInput = QLineEdit()
        self.infraredBitsInput.setText("8")
        layout.addWidget(QLabel("Infrared Bits:"))
        layout.addWidget(self.infraredBitsInput)

        self.resultFolderNameInput = QLineEdit()
        self.resultFolderNameInput.setText(
            f"{datetime.datetime.now().date()}_TestResults_{datetime.datetime.now().strftime('%H%M%S')}")
        layout.addWidget(QLabel("Desired Result Folder Name:"))
        layout.addWidget(self.resultFolderNameInput)

        # Add a button to start the test
        self.startButton = QPushButton("Start Test")
        layout.addWidget(self.startButton)

        self.setLayout(layout)

        # Connect the textChanged and stateChanged signals to the validateInputs function
        self.testDurationInput.textChanged.connect(self.validateInputs)
        self.dataPointsInput.textChanged.connect(self.validateInputs)
        self.resolutionLInput.textChanged.connect(self.validateInputs)
        self.resolutionWInput.textChanged.connect(self.validateInputs)
        self.fpsInput.textChanged.connect(self.validateInputs)
        self.depthBitsInput.textChanged.connect(self.validateInputs)
        self.colorBitsInput.textChanged.connect(self.validateInputs)
        self.infraredBitsInput.textChanged.connect(self.validateInputs)
        self.resultFolderNameInput.textChanged.connect(self.validateInputs)
        self.rgbCheckbox.stateChanged.connect(self.validateInputs)
        self.depthCheckbox.stateChanged.connect(self.validateInputs)
        self.infraredCheckbox.stateChanged.connect(self.validateInputs)

    def validateInputs(self):
        """
        Verify that input fields are not empty and that at least one channel is selected.
        """
        if (self.testDurationInput.text() and self.dataPointsInput.text() and
            self.resolutionLInput.text() and self.resolutionWInput.text() and
            self.fpsInput.text() and self.depthBitsInput.text() and
            self.colorBitsInput.text() and self.infraredBitsInput.text() and
            self.resultFolderNameInput.text() and
                (self.rgbCheckbox.isChecked() or self.depthCheckbox.isChecked() or self.infraredCheckbox.isChecked())):
            self.startButton.setEnabled(True)
        else:
            self.startButton.setEnabled(False)

    def get_testing_parameters(self):
        # Retrieve the values from the input fields and checkboxes
        testDuration = float(self.testDurationInput.text())
        dataPoints = int(self.dataPointsInput.text())
        channels = {
            "RGB": self.rgbCheckbox.isChecked(),
            "Depth": self.depthCheckbox.isChecked(),
            "Infrared": self.infraredCheckbox.isChecked()
        }
        resolution_l = int(self.resolutionLInput.text())
        resolution_w = int(self.resolutionWInput.text())
        fps = int(self.fpsInput.text())
        depthBits = int(self.depthBitsInput.text())
        colorBits = int(self.colorBitsInput.text())
        infraredBits = int(self.infraredBitsInput.text())
        resultFolderName = self.resultFolderNameInput.text()

        return {
            'testDuration': testDuration,
            'dataPoints': dataPoints,
            'channels': channels,
            'resolution_l': resolution_l,
            'resolution_w': resolution_w,
            'fps': fps,
            'depthBits': depthBits,
            'colorBits': colorBits,
            'infraredBits': infraredBits,
            'resultFolderName': resultFolderName
        }


class CameraWidget(QWidget):
    def __init__(self, camera):
        super().__init__()

        mainLayout = QVBoxLayout()
        camInfoGroup = QGroupBox()
        camInfoLayout = QFormLayout()

        camInfoGroup.setStyleSheet("QGroupBox { background-color: #f5f5f5; }")  # Light grey background

        self.serial_number = camera.serial_number
        self.device_name = camera.device_name

        # Create labels for device name and serial number
        name_font = QFont()
        name_font.setPointSize(14)  # Increase font size
        name_font.setBold(True)    # Set font to bold

        self.deviceNameLabel = QLabel(f"Device Name: {self.device_name}")
        self.serialNumberLabel = QLabel(f"Serial Number: {self.serial_number}")

        self.deviceNameLabel.setWordWrap(True)  # Enable word wrapping
        self.serialNumberLabel.setWordWrap(True)  # Enable word wrapping

        self.deviceNameLabel.setFont(name_font)
        self.serialNumberLabel.setFont(name_font)

        self.frameRateLabel = QLabel(f"Frame Rate: 0")
        self.errorCountLabel = QLabel(f"Error Count: 0")
        self.bandwidthLabel = QLabel(f"Bandwidth: 0")

        camInfoLayout.addRow(self.deviceNameLabel)
        camInfoLayout.addRow(self.serialNumberLabel)
        camInfoLayout.addRow(self.frameRateLabel)
        camInfoLayout.addRow(self.errorCountLabel)
        camInfoLayout.addRow(self.bandwidthLabel)

        camInfoGroup.setLayout(camInfoLayout)
        mainLayout.addWidget(camInfoGroup)
        mainLayout.addStretch(1)  # Add a stretch to push the group box to the top
        mainLayout.setSpacing(3)  # Adjust the spacing between widgets

        # Set the size policy of the QGroupBox to be fixed to its minimum required size
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHeightForWidth(camInfoGroup.sizePolicy().hasHeightForWidth())
        camInfoGroup.setSizePolicy(sizePolicy)

        self.setLayout(mainLayout)



    def update_status(self, data_metrics: Dict):
        self.frameRateLabel.setText(
            f"Frame Rate: {data_metrics['frame_rate']:.2f}")
        self.errorCountLabel.setText(
            f"Error Count: {data_metrics['error_count']}")
        self.bandwidthLabel.setText(
            f"Bandwidth: {data_metrics['bandwidth']:.2f}")


class TestingStatusInterface(QWidget):
    def __init__(self, parent=None):
        super(TestingStatusInterface, self).__init__(parent)

        layout = QVBoxLayout()

        # Create a scrollable area
        self.scrollArea = QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        layout.addWidget(self.scrollArea)

        # Create a widget to contain the camera widgets
        self.cameraContainer = QWidget()
        self.cameraLayout = QVBoxLayout()
        self.cameraContainer.setLayout(self.cameraLayout)

        # Set the camera container as the widget for the scrollable area
        self.scrollArea.setWidget(self.cameraContainer)

        # Add a progress bar
        self.progressBar = QProgressBar()
        self.progressBar.setStyleSheet(
            "QProgressBar { background-color: lightgray; } QProgressBar::chunk { background-color: lightblue; }")
        layout.addWidget(self.progressBar)

        # Add a "View Results" button
        self.viewResultsButton = QPushButton("View Results")
        self.viewResultsButton.setEnabled(
            False)  # Disable the button initially
        layout.addWidget(self.viewResultsButton)

        self.setLayout(layout)
