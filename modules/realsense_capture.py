import pyrealsense2 as rs
import time
from multiprocessing import Process, Manager 
from typing import Dict, List

from .data_handler import DataLogger, DataPlotter

class RealsenseCapture:
    def __init__(self, parameters: Dict[str, object], device: rs.device,  publish_signal: List[bool], shared_camera_data: dict):
        self.parameters = parameters
        self.serial_number = device.get_info(rs.camera_info.serial_number)
        self.device_name = device.get_info(rs.camera_info.name)
        
        self.publish_signal = publish_signal
        self.shared_camera_data = shared_camera_data

        self.frame_rate = 0.0
        self.total_frame_errors = 0
        self.bandwidth = 0.0
        self.data_point_counter = 0

        self.data_logging = DataLogger(parameters['resultFolderName'], self.serial_number)
        self.data_vizualization = DataPlotter(parameters['resultFolderName'])

    def _calculate_bandwidth(self):
        # Calculate the bandwidth based on the user-specified parameters in MBPS
        bandwidth = 0.0
        if self.parameters['channels']['RGB']:
            bandwidth += self.parameters['resolution_w'] * self.parameters['resolution_l'] * self.parameters['colorBits']

        if self.parameters['channels']['Depth']:
            bandwidth += self.parameters['resolution_w'] * self.parameters['resolution_l'] * self.parameters['depthBits']

        if self.parameters['channels']['Infrared']:
            bandwidth += self.parameters['resolution_w'] * self.parameters['resolution_l'] * self.parameters['infraredBits'] 

        return  bandwidth * self.frame_rate / 1e6


    def _update_data_metrics(self):
        self.shared_camera_data[self.serial_number] = {
            "frame_rate": self.frame_rate,
            "error_count": self.total_frame_errors,
            "bandwidth": self.bandwidth,
            "data_points_collected": self.data_point_counter
        }

    def _data_capture_loop(self, pipeline: rs.pipeline, config: rs.config):
        try:
            # Start the pipeline
            profile = self.pipeline.start(config)

            #  Here we update metrics on Device Manager signal for the duration of the test
            frame_count = 0
            start_time = time.time()
            next_publish_time = start_time

            while time.time() - start_time < self.parameters['testDuration'] * 3600:
                frames = self.pipeline.wait_for_frames()
                frame_count += 1

                if time.time() >= next_publish_time and self.publish_signal[0]:  # Check the signal
                    self.frame_rate = frame_count / (time.time() - start_time)
                    self.bandwidth = self._calculate_bandwidth()  # Assume this method calculates bandwidth

                    self.data_point_counter += 1  # Increment the data point counter
                    self._update_data_metrics()  # Update the shared dictionary
                    self.data_logging.log_to_csv([self.frame_rate, self.total_frame_errors, self.bandwidth])  # Log the data to a CSV file

                    # Determine the interval for the next publish time
                    interval = self.parameters['testDuration'] * 3600 / self.parameters['dataPoints']
                    next_publish_time += interval

                    self.publish_signal[0] = False  # Reset the signal

        except KeyboardInterrupt:
                return
    

    def run_single_camera_data_logging(self):
        # Create the pipeline object in the child process
        self.pipeline = rs.pipeline()

        # Start the stream based on the user-specified parameters
        config = rs.config()

        if self.parameters['channels']['RGB']:
            config.enable_stream(rs.stream.color, self.parameters['resolution_l'], self.parameters['resolution_w'], rs.format.rgb8, self.parameters['fps'])

        if self.parameters['channels']['Depth']:
            config.enable_stream(rs.stream.depth, self.parameters['resolution_l'], self.parameters['resolution_w'], rs.format.z16, self.parameters['fps'])

        if self.parameters['channels']['Infrared']:
            config.enable_stream(rs.stream.infrared, self.parameters['resolution_l'], self.parameters['resolution_w'], rs.format.y8, self.parameters['fps'])
        
        self._data_capture_loop(self.pipeline, config)

        # Stop the stream
        self.pipeline.stop()

        # Generate the plots
        self.data_vizualization.generate_plots(self.serial_number)


class DeviceManager:
    def __init__(self, parameters: Dict[str, object]):
        self.parameters = parameters
        self.camera_objects = []  # List to keep track of RealsenseCapture objects

        self.manager = Manager()
        self.camera_metrics = self.manager.dict()  # Shared dictionary for cameras to update
        self.publish_signal = self.manager.list([False])  # Shared signal
        
    def enumerate_cameras(self):
        context = rs.context()
        devices = context.query_devices()

        if len(devices) == 0:
            raise Exception('No device detected. Is it plugged in?')

        for device in devices:
            camera = RealsenseCapture(self.parameters, device, self.publish_signal, self.camera_metrics)
            self.camera_objects.append(camera)
        
        return self.camera_objects
    
    def run_multicamera_data_logging(self):
        
        self.processes = []  # Store the processes in an instance variable
        for capture in self.camera_objects: 
            process = Process(target=capture.run_single_camera_data_logging)
            self.processes.append(process)
            process.start()

        # Determine the interval for printing metrics
        interval = self.parameters['testDuration'] * 3600 / self.parameters['dataPoints']
        start_time = time.time()
        next_publish_time = start_time + interval

        while time.time() - start_time < self.parameters['testDuration'] * 3600:
            if time.time() >= next_publish_time:
                self.publish_signal[0] = True  # Signal the child processes to publish updated values
                next_publish_time += interval

        for process in self.processes:
            process.join()
        print("DeviceManager: All camera processes joined")

    def stop_processes(self):
        for process in self.processes:
            process.terminate()  # Terminate the process
            process.join()  # Wait for the process to complete
        
            print("DeviceManager: All camera processes terminated")
