from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import QTimer, QThread
from typing import Dict
import os
import subprocess

from .realsense_capture import DeviceManager
from .gui_components import ParameterInputInterface, TestingStatusInterface, CameraWidget


class RunTestThread(QThread):
    def __init__(self, manager):
        super().__init__()
        self.manager = manager

    def run(self):
        try:
            self.manager.run_multicamera_data_logging()
        except Exception as e:
            print(e)


class App(QMainWindow):
    def __init__(self):
        super().__init__()

         # Set the starting window size
        self.resize(400, 600)          

        # Create the parameter input interface and the testing status interface
        self.parameter_input_interface = ParameterInputInterface(self)
        self.testing_status_interface = TestingStatusInterface(self)

        self.elapsed_time_seconds = 0

        # Initially, set the central widget to the parameter input interface
        self.setCentralWidget(self.parameter_input_interface)

        # Connect the "Start Test" button to the startTest method
        self.parameter_input_interface.startButton.clicked.connect(
            self.run_test_gui)

        self.parameters = None
        self.manager = None

    def _close_event(self, event):
        if self.parameters is not None and self.manager is not None:
            self.manager.stop_processes()  # Call the stop method when the window is closed
        super()._close_event(event)

    def _update_camera_widgets(self):
        for widget in self.testing_status_interface.cameraContainer.children():
            if isinstance(widget, CameraWidget):
                serial_number = widget.serial_number
                if serial_number in self.manager.camera_metrics:
                    widget.update_status(self.manager.camera_metrics[serial_number])

    def _update_progress_bar(self):
        currentValue = self.testing_status_interface.progressBar.value()
        max_value = int(self.parameters["testDuration"] * 3600)

        all_data_points_collected = all(
            camera_data.get("data_points_collected", 0) == self.parameters['dataPoints']
            for camera_data in self.manager.camera_metrics.values()
        )

        if currentValue < max_value:
            self.testing_status_interface.progressBar.setValue(currentValue + 1)
            self._update_camera_widgets()  # Update the camera widgets

        elif currentValue == max_value:
            if all_data_points_collected:
                # If all data points are collected and test duration has been met, set the progress bar to maximum
                self.testing_status_interface.progressBar.setValue(max_value)
                # The test is complete
                self.testing_status_interface.viewResultsButton.setEnabled(True)  # Enable the "View Results" button

            else: 
                self.timer.stop()  # Stop the timer

    def _open_results(self):
        """
        Method to open the results folder in the file explorer.
        """
        if self.parameters is not None:
            try:
                resultFolderName = self.parameters['resultFolderName']
                topLevelFolder = "TestingResults"
                resultsFolder = os.path.join(topLevelFolder, resultFolderName)

                # Check if the folder exists
                if os.path.exists(resultsFolder):
                    # Open the folder using the default file explorer
                    if os.name == 'nt':  # Windows
                        subprocess.run(['explorer', resultsFolder])
                    elif os.name == 'posix':  # macOS and Linux
                        subprocess.run(['open', resultsFolder])
                    else:
                        print(f"Could not open the folder: {resultsFolder}. Unsupported OS.")
                else:
                    print(f"The folder {resultsFolder} does not exist.")

            except Exception as e:
                print(f"An error occurred while trying to open the results folder: {e}")


    def run_test_gui(self):
        try:
            # Retrieve the values from the input fields and checkboxes
            self.parameters: Dict = self.parameter_input_interface.get_testing_parameters()

            # Create DeviceManager instance with the parameters
            self.manager = DeviceManager(self.parameters)

            cameras = []
            cameras = self.manager.enumerate_cameras()
            for camera in cameras:
                widget = CameraWidget(camera)
                self.testing_status_interface.cameraLayout.addWidget(widget)

            # Create and start the thread
            self.devices_thread = RunTestThread(self.manager)

            # Start the thread
            self.devices_thread.start()

            # Switch to the testing status interface
            self.setCentralWidget(self.testing_status_interface)

            # Set the maximum value of the progress bar to the test duration in seconds
            self.testing_status_interface.progressBar.setMaximum(
                int(self.parameters["testDuration"] * 3600))

            # Create a QTimer that updates the progress bar every second
            self.timer = QTimer()
            self.timer.timeout.connect(self._update_progress_bar)
            self.timer.start(1000)  # 1000 ms = 1 s

            # Connect the "View Results" button to the open_results method
            self.testing_status_interface.viewResultsButton.clicked.connect(self._open_results)


        except Exception as e:
            print(e)
            self.close()  # Close the application