import csv
import os
from typing import List
from datetime import datetime
import matplotlib.pyplot as plt

class DataLogger:
    def __init__(self, resultFolderName, cam_serial_number: str):
        self.topLevelFolder = "TestingResults"
        self.resultFolderName = resultFolderName
        self.cam_serial_number = cam_serial_number

    def log_to_csv(self, data: List):
        # Create the results folder if it doesn't exist
        if not os.path.exists(self.topLevelFolder):
            os.mkdir(self.topLevelFolder)

        # Create the results folder
        resultsFolder = os.path.join(self.topLevelFolder, self.resultFolderName)
        if not os.path.exists(resultsFolder):
            os.mkdir(resultsFolder)

        # Create the CSV file if it doesn't exist
        csvFile = os.path.join(resultsFolder, f"{self.cam_serial_number}_stream_data.csv")
        csvExists = os.path.exists(csvFile)
        with open(csvFile, 'a', newline='') as file:
            writer = csv.writer(file)
            if not csvExists:
                writer.writerow(["timestamp", "frame_rate", "error_count", "bandwidth"])  # Added timestamp
            writer.writerow([datetime.now().isoformat(), *data])  # Added timestamp

class DataPlotter:
    def __init__(self, resultFolderName):
        self.topLevelFolder = "TestingResults"
        self.resultFolderName = resultFolderName

    def generate_plots(self, serial_number):
        # Path to the CSV file
        csvFile = os.path.join(self.topLevelFolder, self.resultFolderName, f"{serial_number}_stream_data.csv")

        # Check if the CSV file exists
        if not os.path.exists(csvFile):
            print(f"CSV file for serial number {serial_number} does not exist.")
            return

        # Read the CSV file
        timestamps = []
        frame_rates = []
        error_counts = []
        bandwidths = []
        with open(csvFile, 'r') as file:
            reader = csv.reader(file)
            next(reader)  # Skip the header row
            for row in reader:
                timestamp, frame_rate, error_count, bandwidth = row
                timestamps.append(datetime.fromisoformat(timestamp))  # Convert timestamp from ISO format
                frame_rates.append(float(frame_rate))
                error_counts.append(float(error_count))
                bandwidths.append(float(bandwidth))

        # Create a figure with three subplots
        fig, axs = plt.subplots(3, 1, figsize=(10, 15))

        # Plot the frame rates
        axs[0].plot(timestamps, frame_rates)
        axs[0].set_title(f"Frame Rates for Camera {serial_number}")
        axs[0].set_xlabel('Time')
        axs[0].set_ylabel('Frame Rate')

        # Plot the error counts
        axs[1].plot(timestamps, error_counts)
        axs[1].set_title(f"Error Counts for Camera {serial_number}")
        axs[1].set_xlabel('Time')
        axs[1].set_ylabel('Error Count')

        # Plot the bandwidths
        axs[2].plot(timestamps, bandwidths)
        axs[2].set_title(f"Bandwidths for Camera {serial_number}")
        axs[2].set_xlabel('Time')
        axs[2].set_ylabel('Bandwidth')

        # Save the figure as a PNG file
        plt.savefig(os.path.join(self.topLevelFolder, self.resultFolderName, f"{serial_number}_plots.png"))

        # Close the figure to release resources
        plt.close(fig)
